# ExerciciosLivroManzano

Este projeto terá exercicios e soluções dos problemas propostos no livro "Introdução á linguagem Lua"

### Capitulo 2 - Introdução:

1: Faça um conversor de temperaturas Celsius para Farenheit

2: Faça um conversor de temperaturas Farenheit para Celsius

3: Troca de valores (só trocar os valores de duas variaveis)

4: Calcular e apresentar o volume de uma caixa retangular usando a formula vol = comprimento * largura * altura

5: Ler um valor numerico e apresentar o valor ao quadrado

6: Ler dois valores e apresentar o resultado do quadrado da diferença entre os valores

7: Ler tres valores e apresentar o valor da soma dos quadrados dos valores lidos

8: Ler tres valores e apresentar o valor do quadrado da soma dos valores

9: Elaborar um programa que calcule e apresente o valor do resultado da area de uma circunferencia (variavel VOL), deve solicitar o valor do raio da circunferencia e usar a formula VOL = math.pi+R^2

10: Elaborar um programa queleia dois valores e apresentar o resultado das 4 operações matemáticas (add, sub, mul, div)

11: Elaborar um programa que calcula uma raiz de base com indice qualquer
