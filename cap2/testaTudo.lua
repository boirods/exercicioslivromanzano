local m = require 'ex1Functions'

local testaTempCelsius = 5
print('Temperatura: '..testaTempCelsius)
print('Temperatura em fahrenheit: '..m.celsius2Fahrenheit(testaTempCelsius))
print('Temperatura fahrenheit em celsius: '..m.fahrenheit2Celsius(m.celsius2Fahrenheit(testaTempCelsius))..'\n')

local testaDoisValores = 7
print('Valores: '.. testaTempCelsius..'\t'..testaDoisValores)
testaTempCelsius,testaDoisValores = m.troca(testaTempCelsius, testaDoisValores)
print('Valores trocados: '..testaTempCelsius..'\t'..testaDoisValores..'\n')

local terceiroValor = 9
print('Testando o volume de uma caixa')
print('Uma caixa com com largura '..terceiroValor..' com comprimento '..testaDoisValores..' e com altura '..testaTempCelsius)
print('O volume da caixa é: '..m.volume(testaDoisValores, terceiroValor, testaTempCelsius)..'\n')

print(testaTempCelsius ..' ^ 2 = '..m.aoQuadrado(testaTempCelsius))

print('O quadrado da diferença de '..testaDoisValores..' e '..terceiroValor..' é: '..m.quadradoDaDiferenca(testaDoisValores, terceiroValor))

print('A soma dos quadrados de '..testaTempCelsius..', '..testaDoisValores..' e '..terceiroValor..' é: '..m.somaDosQuadrados(testaTempCelsius, testaDoisValores, terceiroValor))

print('O quadrado das somas dos valores '..testaTempCelsius..', '..testaDoisValores..' e '..terceiroValor..' é: '..m.quadradoDaSoma(testaTempCelsius, testaDoisValores, terceiroValor)..'\n')

print('A área de um circulo com raio '.. testaTempCelsius ..' é: '..m.areaCirc(testaTempCelsius)..'\n')
print(m.arit(testaDoisValores, terceiroValor))

print(testaDoisValores..' elevado a '..terceiroValor..'é: '..m.raiz(testaDoisValores,terceiroValor))