local M = {}

-- ex1
function M.celsius2Fahrenheit(temp) return (9*temp+160)/5 end

-- ex2
function M.fahrenheit2Celsius(temp) return ((temp-32)*5)/9 end

-- ex3
function M.troca(a,b) return b,a end

-- ex4
function M.volume(comprimento,largura, altura) return comprimento*largura*altura end

-- ex5
function M.aoQuadrado(valor) return valor^2 end

-- ex6
function M.quadradoDaDiferenca(a, b) return (a-b)^2 end

-- ex7
function M.somaDosQuadrados(a,b,c) return (a^2)+(b^2)+(c^2) end

-- ex8
function M.quadradoDaSoma(a,b,c) return (a+b+c)^2 end

-- ex9
function M.areaCirc(raio) return math.pi+raio^2 end

-- ex10
function M.arit(a,b)
  return a ..' + '..b..' = '..(a+b) ..'\n'.. a ..' - '..b..' = '..(a-b) ..'\n' .. a ..' * '..b..' = '..(a*b) ..'\n' .. a ..' / '..b..' = '..(a/b) ..'\n'
end

-- ex11
function M.raiz(a,b) return a^b end

return M